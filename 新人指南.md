# <center>新人指南</center>
#### <center>作者：陌生人</center>
#### <center>2022-04-15 13:43:05</center>
此文档和腾讯文档的[新人手册](https://docs.qq.com/sheet/DWEV2Z3dTZWJPZ3pl)进行双向同步，部分可能会有版权问题的内容会放在腾讯文档中
|   名称|  链接 |备注|来源|
| ------------ | ------------ |------------ |------------ |
|  优麒麟百科 |  https://ubuntukylin.github.io/ |搭建在github上，访问可能有些慢，可以多试几次，里边也有很多问答，可以看下有没有自己需要的|优麒麟官方|
| 2004安装图文  | https://mp.weixin.qq.com/s/h7Lcyb5-PqB0q-pHUINCug  |暂无 | 优麒麟官方公众号  |
| 2004安装视频  | https://www.bilibili.com/video/BV1TP4y1x7fR/  | | 陌生人  |
|  1804安装视频 |  https://www.bilibili.com/video/BV1M7411v7v2/ |防止在2004的双安装器里误用了1804的安装器 | 陌生人  |
| 懒人版优麒麟操作系统20.04 LTS PRO  | https://www.ubuntukylin.com/ukylin/forum.php?mod=viewthread&tid=194554  | |  优麒麟论坛 |
|  Q&A（常见问题） | [Q&A](./%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98/QA.md)  |常见问题说明，大多是一些大家不明白的问题 | 陌生人  |
| 银河麒麟Linux系统从入门到办公到精通\|国产操作系统\|优麒麟\|UOS\|Deepin  | https://www.bilibili.com/video/BV1Hv411p7HG?spm_id_from=333.999.0.0  |系列视频，比较长，目前大部分银河，部分优麒麟 | Linux重度使用  |
|  为什么 WPS 和 Office 中显示的版式不一致？ | https://my.oschina.net/chipo/blog/5044536  | |  chipo |
|  关机或重启等待90秒（1分30秒）的问题 | https://my.oschina.net/chipo/blog/5062815  | | chipo  |

